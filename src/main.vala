/* main.vala
 *
 * Copyright 2023 Andrés Fernández
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
using Gee;

int main (string[] args)
{
	int arreglo_falso[10] = {6,8,7,9,6,7,5,9,9,9};
  int arreglo_verdadero[10] = {6,8,7,9,6,7,5,2,1,9};
  int suma_buscada = 8;
  string resultado = "";

  resultado = traducir_valor_bool (busqueda_segundo_sumando (arreglo_falso, suma_buscada));
  print ("\nFunción de búsqueda no optimizada");
  print ("\nArreglo: {6,8,7,9,6,7,5,9,9,9}");
  print ("\n \t Es " + resultado + " que este array contiene dos números que sumados dan como resultado " + suma_buscada.to_string () +".\n");
  resultado = traducir_valor_bool (busqueda_segundo_sumando (arreglo_verdadero, suma_buscada));
  print ("\nArreglo: {6,8,7,9,6,7,5,9,9,9}");
  print ("\n \t Es " + resultado + " que este array contiene dos números que sumados dan como resultado " + suma_buscada.to_string () +".\n");

  resultado = traducir_valor_bool (busqueda_optimizada_segundo_sumando (arreglo_falso, suma_buscada));
  print ("\nFunción de búsqueda optimizada");
  print ("\nArreglo: {6,8,7,9,6,7,5,9,9,9}");
  print ("\n \t Es " + resultado + " que este array contiene dos números que sumados dan como resultado " + suma_buscada.to_string () +".\n");
  resultado = traducir_valor_bool (busqueda_optimizada_segundo_sumando (arreglo_verdadero, suma_buscada));
  print ("\nArreglo: {6,8,7,9,6,7,5,9,9,9}");
  print ("\n \t Es " + resultado + " que este array contiene dos números que sumados dan como resultado " + suma_buscada.to_string () +".\n");

  return 0;
}

/* La función busca en el arreglo que se pasa en el primer parámetro si existen
 * dos números que sumados den el valor establecido en el segundo parámetro de
 * la función. Recorre el arreglo buscando por cada elemento si contiene el
 * segundo sumando.
 */
bool busqueda_segundo_sumando (int arreglo[10], int suma_buscada) {
  bool resultado = false;
  int segundo_sumando = 0;
  // Se recorre el arreglo hasta encontrar dos números que sumen la suma_buscada
  for (int i = 0; i < arreglo.length; i++) {
    // Se calcula el número necesario para lograr la suma deseada para cada
    // ítem del arreglo.
    segundo_sumando = calcular_segundo_sumando (arreglo[i], suma_buscada);
    // Se busca en el arreglo desde la posición ya revisada para evitar
    // que verifiquen números ya verificados anteriormente.
    for (int j=i+1; j < arreglo.length; j++) {
      if (arreglo[j] == segundo_sumando ) {
        resultado = true;
        // Una vez determinada la existencia de un segundo sumando se termina
        // el ciclo.
        break;
      }
    }
    // Una vez determinada la existencia de un segundo sumando se termina
    // el ciclo.
    if (resultado == true){
      break;
    }
  }

  return resultado;
}

/* La función busca en el arreglo pasado en el primer parámetro el segundo
 * sumando de cada ítem del mismo. Lo hace como máximo en una recorrida del
 * mismo, haciendo uso de una lista que guarda los ítems del arreglo ya
 * verificados.
 */
bool busqueda_optimizada_segundo_sumando (int arreglo[10], int suma_buscada) {
  bool resultado = false;
  int segundo_sumando = 0;
  Gee.ArrayList<int> list = new Gee.ArrayList<int> ();

  for (int i=0; i < arreglo.length; i++) {
    segundo_sumando = calcular_segundo_sumando (arreglo[i], suma_buscada);
    // Por cada iteración del ciclo for, el algoritmo revisa si el segundo
    // sumando se encuentra agregado a la lista. De no ser así, guarda en la
    // lista el ítem del arreglo correspondiente a esta iteración del ciclo.
    if (list.contains (segundo_sumando)){
      resultado = true;
      break;
    } else {
      list.add (arreglo[i]);
    }
  }

  return resultado;
}

/* Calcula el número necesario para que sumado al número entero del primer
 * parámetro se tenga como resultado el valor establecido en el segundo
 * parámetro.
 */
int calcular_segundo_sumando (int numero, int suma_buscada) {
  return suma_buscada - numero;
}

/* Esta función devuelve un valor string con la palabra en castellano
 * correspondiente al valor de la variable bool pasada por comando.
 * Dado que, si bien, el lenguaje vala cuanta con la opción para traducir a
 * string el valor de una variable bool, lo hace en inglés. Para traducirlo al
 * castellano dinámicamente habría que hacer uso de la infraestructura de
 * internacionalizión (i18n), que excede el tamaño del proyecto.
 */
string traducir_valor_bool (bool valor) {
  string resultado = "falso";

  if (valor == true) {
    resultado = "verdadero";
  }

  return resultado;
}

